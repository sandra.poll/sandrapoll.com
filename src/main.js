import '@fortawesome/fontawesome-free/css/all.min.css'
import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'

import Home from './pages/Home'

Vue.use(VueRouter)

import './assets/main.css'

import '@vuikit/theme'
import vuetify from './plugins/vuetify';
import Projects from "@/pages/Projects";
import Contact from "@/pages/Contact";
import colors from 'vuetify/lib/util/colors'
import Vuetify from "vuetify";

// eslint-disable-next-line no-undef
export default new Vuetify({
  theme: {
    options: {
      themeCache: {
        get: key => localStorage.getItem(key),
        set: (key, value) => localStorage.setItem(key, value),
      },
    },
    themes: {
      light: {
        primary: colors.purple,
        secondary: colors.grey.darken1,
        accent: colors.shades.black,
        error: colors.red.accent3,
      },
      dark: {
        primary: colors.blue.lighten3,
      },
    },
  },
})

const routes = [
  { path: '/', component: Home },
  { path: '/projects', component: Projects },
  { path: '/contact', component: Contact }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  vuetify,
  router
}).$mount('#app')
